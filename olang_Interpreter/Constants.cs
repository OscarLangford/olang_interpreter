﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace olang_Interpreter
{
    public static class Constants
    {
        public enum OC
        {
            NUL,
            MOVA,
            MOVB,
            ADDA,
            ADDB,
            SUBA,
            SUBB,
            INPN,
            INPC,
            OUTN,
            OUTC,
            LBL,
            JMP,
            JEZA,
            JEZB,
            JSR,
            RET,
            CHN,
            END
        };

        public enum RS
        {
            OK,
            FILE_ERROR,
            PARSING_ERROR,
            DUPLICATE_LABEL_NAMES,
            DUPLICATE_INPUT_CHARS,
            INCORRECT_INPUT,
            LABEL_NOT_FOUND,
            NO_RETURN_POINT,
            UNRECOGNISED_OPCODE,
            CODE_ERROR
        };
    }
}
