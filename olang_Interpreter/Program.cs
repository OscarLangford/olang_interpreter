﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static olang_Interpreter.Constants;

namespace olang_Interpreter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(Console.WindowWidth * 2 - 20, Console.WindowHeight * 2);

            Interpreter interpreter = new Interpreter();

            Console.Write("---- olang Interpreter v0.1a ----\n");
            bool startAgain = true;

            while (startAgain)
            {
                bool directCode = false, debugOutput = false, runAgain = true;
                var input = 'c';

                while (input == 'c')
                {
                    Console.Write("\nPress 'a' to load a file or 'b' to enter code manually: ");
                    var keyPressed = Console.ReadKey().KeyChar;
                    if (keyPressed == 'a')
                    {
                        directCode = false;
                        input = keyPressed;
                    }
                    if (keyPressed == 'b')
                    {
                        directCode = true;
                        input = keyPressed;
                    }
                }
                Console.WriteLine();
                input = 'c';
                while (input == 'c')
                {
                    Console.Write("\nPress 'a' for standard output or 'b' for output with debugging info: ");
                    var keyPressed = Console.ReadKey().KeyChar;
                    if (keyPressed == 'a')
                    {
                        debugOutput = false;
                        input = keyPressed;
                    }
                    if (keyPressed == 'b')
                    {
                        debugOutput = true;
                        input = keyPressed;
                    }
                }

                Console.Write($"\n\nPlease enter {(directCode ? "code: " : "file location: ")}");
                var newFile = Console.ReadLine();
                Console.WriteLine();


                while (runAgain)
                {
                    RS returnStatus = interpreter.AddNewFile(newFile, directCode);
                    RS parseStatus = RS.OK;

                    if (returnStatus == RS.OK) parseStatus = interpreter.Parse();
                    else
                    {
                        Console.WriteLine("The program returned status: " + returnStatus);
                        Console.WriteLine("\nPress any key to exit.");
                        Console.ReadKey();
                        Environment.Exit(0);
                    }

                    if (parseStatus == RS.OK)
                    {
                        Console.WriteLine("Output:\n");
                        returnStatus = interpreter.Run(debugOutput);
                        Console.WriteLine("The program returned status: " + returnStatus);
                        var inp = 'd';
                        while (inp == 'd')
                        {
                            Console.Write("\nPress 'a' to run again, 'b' to start over, or 'c' to exit: ");
                            var key = Console.ReadKey().KeyChar;
                            if (key == 'a')
                            {
                                inp = key;
                                Console.WriteLine("\n");
                            }
                            if (key == 'b')
                            {
                                inp = key;
                                runAgain = false;
                                Console.WriteLine();
                            }
                            if (key == 'c')
                            {
                                Console.WriteLine("\n");
                                inp = key;
                                runAgain = false;
                                startAgain = false;
                            }
                        }
                    }
                }
            }
        }
    }
}
