﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static olang_Interpreter.Constants;

namespace olang_Interpreter
{    public class Operation
    {
        private string rawCode;
        public OC Code { get; set; }
        public int Value { get; set; }
        public int InputType { get; private set; }

        public Operation(string data)
        {
            rawCode = data;
        }
        public string GetRawCode()
        {
            return rawCode;
        }
        public void SetInputType(char a, char b, char value)
        {
            if (value == a) InputType = 1;
            if (value == b) InputType = 2;
        }
    }
}
