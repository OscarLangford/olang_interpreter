﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace olang_Interpreter
{
    // I created this class to hold all the input and output commands. This way, it's easier to port the logic over to other types of UI
    public static class InOut
    {
        public static void Write(string input) { Console.Write(input); }
        public static void WriteLine() { Console.WriteLine(); }
        public static void WriteLine(string input) { Console.WriteLine(input); }
        public static char ReadKey() { return Console.ReadKey().KeyChar; }
        public static string ReadLine() { return Console.ReadLine(); }
    }
}
