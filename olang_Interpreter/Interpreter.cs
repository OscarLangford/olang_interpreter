﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static olang_Interpreter.Constants;

namespace olang_Interpreter
{
    // this is the main class. Takes the file, cuts it up, and populates the operations list
    public class Interpreter
    {
        private List<Operation> ops = new List<Operation>();
        private string file;
        private char[] inputChars;
        private int addValue = 0;
        private List<KeyValuePair<int, int>> boxValues = new List<KeyValuePair<int, int>>();
        private List<string> debugLines = new List<string>();
        private List<string> parsedLines = new List<string>();
        private const int TAB_GAP = 20;
        private bool debug = false;

        // clears the old list first, so it can be resused
        public RS AddNewFile(string inputFile, bool directCode = false)
        {
            try
            {
                string input;

                if (!directCode)
                {
                    using (StreamReader sr = new StreamReader(inputFile))
                    {
                        input = sr.ReadToEnd();
                    }
                }
                else input = inputFile;

                file = input;
                return RS.OK;
            }
            catch
            {
                return RS.FILE_ERROR;
            }

        }

        // reads the file string and splits it up, then parses the smaller strings and populates the ops List parameters
        public RS Parse()
        {

            debugLines.Clear();
            parsedLines.Clear();
            ops.Clear();
            addValue = 0;
            inputChars = new char[4];
            List<string> opStrings = new List<string>();
            List<int> labelNames = new List<int>();
            int stringStart = 3;
            bool inComment = false;
            StringBuilder legitCharacters = new StringBuilder();

            // check that the first characters are unique
            if (file[0] == file[1] || file[0] == file[2] || file[1] == file[2]) return RS.DUPLICATE_INPUT_CHARS;

            for (int i = 0; i < file.Length; i++)
            {
                // set the 3 input characters, plus the comment character, if there is one
                if (i == 0 || i == 1 || i == 2) inputChars[i] = file[i];
                if (i == 3)
                {
                    if (file[i] != inputChars[0] && file[i] != inputChars[1] && file[i] != inputChars[2])
                    {
                        inputChars[3] = file[i];
                        stringStart++;
                        continue;
                    }
                    else inputChars[3] = '\0';
                }

                if (i > 2)
                {

                    if (file[i] == inputChars[0] || file[i] == inputChars[1] || file[i] == inputChars[2])
                    {
                        if (file[i] == inputChars[2] && legitCharacters.Length > 2)
                        {
                            opStrings.Add(legitCharacters.ToString());
                            legitCharacters.Clear();
                        }
                        else
                        {
                            if (!inComment) legitCharacters.Append(file[i]);
                        }

                    }

                }

                if (i > 3)
                {
                    if (inputChars[3] != '\0' && file[i] == inputChars[3]) inComment = !inComment;
                }
            }

            // parse the strings and populate the ops List
            foreach (var opString in opStrings)
            {
                Operation op = new Operation(opString);
                //Console.WriteLine(opString);

                string first3Chars = opString.Substring(0, 3);

                // TODO return an error if people enter input type C at any point, as there isn't one
                // TODO print debug info on parsing error
                if (first3Chars == $"{inputChars[0]}{inputChars[0]}{inputChars[0]}") // aaa
                {
                    if (opString[3] == inputChars[0]) op.Code = OC.MOVA;
                    else op.Code = OC.MOVB;

                    op.SetInputType(inputChars[0], inputChars[1], opString[4]);
                    if (opString.Length > 5)
                    {
                        op.Value = GetNumberFromString(opString.Substring(5, opString.Length - 5));
                    }
                }

                else if (first3Chars == $"{inputChars[1]}{inputChars[1]}{inputChars[1]}") // bbb
                {
                    if (opString[3] == inputChars[0]) op.Code = OC.ADDA;
                    else op.Code = OC.ADDB;

                    op.SetInputType(inputChars[0], inputChars[1], opString[4]);
                    if (opString.Length > 5)
                    {
                        op.Value = GetNumberFromString(opString.Substring(5, opString.Length - 5));
                    }
                }

                else if (first3Chars == $"{inputChars[1]}{inputChars[1]}{inputChars[0]}") // bba
                {
                    if (opString[3] == inputChars[0]) op.Code = OC.SUBA;
                    else op.Code = OC.SUBB;

                    op.SetInputType(inputChars[0], inputChars[1], opString[4]);
                    if (opString.Length > 5)
                    {
                        op.Value = GetNumberFromString(opString.Substring(5, opString.Length - 5));
                    }
                }

                else if (first3Chars == $"{inputChars[2]}{inputChars[2]}{inputChars[2]}") // ccc
                {
                    op.Code = OC.LBL;
                    if (opString.Length > 3)
                    {
                        op.Value = GetNumberFromString(opString.Substring(3, opString.Length - 3));
                    }

                    if (labelNames.Contains(op.Value)) return RS.DUPLICATE_LABEL_NAMES;
                    else labelNames.Add(op.Value);
                }

                else if (first3Chars == $"{inputChars[2]}{inputChars[2]}{inputChars[0]}") // cca
                {
                    op.Code = OC.JMP;
                    if (opString.Length > 3)
                    {
                        op.Value = GetNumberFromString(opString.Substring(3, opString.Length - 3));
                    }
                }

                else if (first3Chars == $"{inputChars[2]}{inputChars[2]}{inputChars[1]}") // ccb
                {
                    if (opString[3] == inputChars[0]) op.Code = OC.JEZA;
                    else op.Code = OC.JEZB;

                    if (opString.Length > 4)
                    {
                        op.Value = GetNumberFromString(opString.Substring(4, opString.Length - 4));
                    }
                }

                else if (first3Chars == $"{inputChars[2]}{inputChars[1]}{inputChars[2]}") // cbc
                {
                    op.Code = OC.JSR;
                    if (opString.Length > 3)
                    {
                        op.Value = GetNumberFromString(opString.Substring(3, opString.Length - 3));
                    }
                }

                else if (first3Chars == $"{inputChars[2]}{inputChars[1]}{inputChars[0]}") // cba
                {
                    op.Code = OC.RET;
                    if (opString.Length > 3)
                    {
                        return RS.PARSING_ERROR;
                    }
                }

                else if (first3Chars == $"{inputChars[0]}{inputChars[0]}{inputChars[1]}") // aab
                {
                    if (opString[3] == inputChars[0]) op.Code = OC.INPN;
                    else op.Code = OC.INPC;

                    if (opString.Length > 4)
                    {
                        return RS.PARSING_ERROR;
                    }
                }

                else if (first3Chars == $"{inputChars[0]}{inputChars[0]}{inputChars[2]}") // aac
                {
                    if (opString[3] == inputChars[0]) op.Code = OC.OUTN;
                    else op.Code = OC.OUTC;

                    if (opString.Length > 4)
                    {
                        return RS.PARSING_ERROR;
                    }
                }

                else if (first3Chars == $"{inputChars[1]}{inputChars[0]}{inputChars[2]}") // bac
                {
                    op.Code = OC.END;
                }

                else return RS.PARSING_ERROR;

                ops.Add(op);
            }

            // for debug purposes
            foreach (var entry in ops)
            {
                StringBuilder parsedLine = new StringBuilder();

                parsedLine.Append(entry.GetRawCode());
                for (int i = 0; i < TAB_GAP - entry.GetRawCode().Length; i++)
                {
                    parsedLine.Append(" ");
                }

                parsedLine.Append($"{entry.Code}" + " ");
                if (entry.InputType == 2) parsedLine.Append("(");
                parsedLine.Append(entry.Value);
                if (entry.InputType == 2) parsedLine.Append(")");
                parsedLines.Add(parsedLine.ToString());
            }

            return RS.OK;
        }

        // returns an integer from a binary string of input chararacters
        private int GetNumberFromString(string s)
        {
            int count = 0;
            int number = 0;
            for (int i = s.Length - 1; i >= 0; i--)
            {
                if (s[i] == inputChars[0])
                {
                    number += Convert.ToInt32((Math.Pow(2, count)));
                }
                count++;
            }

            return number;
        }

        // runs the code, now that it has been parsed
        public RS Run(bool debugOutput)
        {
            List<int> returnLines = new List<int>();
            debug = debugOutput;

            // the important part that goes through each op and runs the code
            for (int i = 0; i < ops.Count; i++)
            {
                Operation op = ops[i];

                switch (op.Code)
                {
                    case OC.MOVA:
                        if (op.InputType == 1) addValue = op.Value;
                        if (op.InputType == 2)
                        {
                            foreach (var boxVal in boxValues)
                            {
                                if (boxVal.Key == op.Value) addValue = boxVal.Value;
                            }
                        }
                        SaveDebugInfo(i);
                        break;

                    case OC.MOVB:
                        for (int j = 0; j < boxValues.Count; j++)
                        {
                            if (boxValues[j].Key == addValue)
                                boxValues.RemoveAt(j);
                        }

                        if (op.InputType == 1)
                        {
                            boxValues.Add(new KeyValuePair<int, int>(addValue, op.Value));
                        }
                        if (op.InputType == 2)
                        {
                            boxValues.Add(new KeyValuePair<int, int>(addValue, GetValueAtBoxAddress(op.Value)));
                        }
                        SaveDebugInfo(i);
                        break;

                    case OC.INPN:
                        for (int j = 0; j < boxValues.Count; j++)
                        {
                            if (boxValues[j].Key == addValue)
                                boxValues.RemoveAt(j);
                        }
                        int num;
                        if (int.TryParse(InOut.ReadLine(), out num))
                        {
                            boxValues.Add(new KeyValuePair<int, int>(addValue, num));
                            op.Value = num;
                        }
                        else
                        {
                            PrintDebugInfo();
                            return RS.INCORRECT_INPUT;
                        }
                        SaveDebugInfo(i);
                        break;

                    case OC.INPC:
                        for (int j = 0; j < boxValues.Count; j++)
                        {
                            if (boxValues[j].Key == addValue)
                                boxValues.RemoveAt(j);
                        }
                        int num2 = InOut.ReadKey();
                        boxValues.Add(new KeyValuePair<int, int>(addValue, num2));
                        op.Value = num2;
                        SaveDebugInfo(i);
                        break;

                    case OC.OUTN:
                        InOut.Write(GetValueAtBoxAddress(addValue).ToString());
                        op.Value = GetValueAtBoxAddress(addValue);
                        SaveDebugInfo(i);
                        break;

                    case OC.OUTC:
                        InOut.Write(Convert.ToChar(GetValueAtBoxAddress(addValue)).ToString());
                        op.Value = Convert.ToChar(GetValueAtBoxAddress(addValue));
                        SaveDebugInfo(i);
                        break;

                    case OC.ADDA:
                        if (op.InputType == 1) addValue += op.Value;
                        if (op.InputType == 2)
                        {
                            foreach (var boxVal in boxValues)
                            {
                                if (boxVal.Key == op.Value) addValue += boxVal.Value;
                            }
                        }
                        SaveDebugInfo(i);
                        break;

                    case OC.ADDB:
                        int deletedValue = 0;
                        for (int j = 0; j < boxValues.Count; j++)
                        {
                            if (boxValues[j].Key == addValue)
                            {
                                deletedValue = boxValues[j].Value;
                                boxValues.RemoveAt(j);
                            }
                        }

                        if (op.InputType == 1)
                        {
                            boxValues.Add(new KeyValuePair<int, int>(addValue, op.Value + deletedValue));
                        }
                        if (op.InputType == 2)
                        {
                            boxValues.Add(new KeyValuePair<int, int>(addValue, GetValueAtBoxAddress(op.Value) + deletedValue));
                        }
                        SaveDebugInfo(i);
                        break;

                    case OC.SUBA:
                        if (op.InputType == 1) addValue -= op.Value;
                        if (op.InputType == 2)
                        {
                            foreach (var boxVal in boxValues)
                            {
                                if (boxVal.Key == op.Value) addValue -= boxVal.Value;
                            }
                        }
                        if (addValue < 0) addValue = 0;
                        SaveDebugInfo(i);
                        break;

                    case OC.SUBB:
                        int deletedValue2 = 0;
                        for (int j = 0; j < boxValues.Count; j++)
                        {
                            if (boxValues[j].Key == addValue)
                            {
                                deletedValue2 = boxValues[j].Value;
                                boxValues.RemoveAt(j);
                            }
                        }

                        if (op.InputType == 1)
                        {
                            boxValues.Add(new KeyValuePair<int, int>(addValue, deletedValue2 - op.Value));
                        }
                        if (op.InputType == 2)
                        {
                            boxValues.Add(new KeyValuePair<int, int>(addValue, deletedValue2 - GetValueAtBoxAddress(op.Value)));
                        }
                        SaveDebugInfo(i);
                        break;

                    case OC.LBL:
                        SaveDebugInfo(i);
                        break;

                    case OC.JMP:
                        bool foundJmpLabel = false;
                        for (int j = 0; j < ops.Count; j++)
                        {
                            if (ops[j].Code == OC.LBL && ops[j].Value == op.Value)
                            {
                                i = j;
                                foundJmpLabel = true;
                            }
                        }
                        if (!foundJmpLabel)
                        {
                            PrintDebugInfo();
                            return RS.LABEL_NOT_FOUND;
                        }

                        SaveDebugInfo(i);
                        break;

                    case OC.JEZA:
                        if (addValue == 0)
                        {
                            bool foundJmpLabel2 = false;
                            for (int j = 0; j < ops.Count; j++)
                            {
                                if (ops[j].Code == OC.LBL && ops[j].Value == op.Value)
                                {
                                    i = j;
                                    foundJmpLabel2 = true;
                                }
                            }
                            if (!foundJmpLabel2)
                            {
                                PrintDebugInfo();
                                return RS.LABEL_NOT_FOUND;
                            }
                        }

                        SaveDebugInfo(i);
                        break;

                    case OC.JEZB:
                        if (GetValueAtBoxAddress(addValue) == 0)
                        {
                            bool foundJmpLabel3 = false;
                            for (int j = 0; j < ops.Count; j++)
                            {
                                if (ops[j].Code == OC.LBL && ops[j].Value == op.Value)
                                {
                                    i = j;
                                    foundJmpLabel3 = true;
                                }
                            }
                            if (!foundJmpLabel3)
                            {
                                PrintDebugInfo();
                                return RS.LABEL_NOT_FOUND;
                            }
                        }

                        SaveDebugInfo(i);
                        break;

                    case OC.JSR:
                        bool foundJmpLabel4 = false;
                        for (int j = 0; j < ops.Count; j++)
                        {
                            if (ops[j].Code == OC.LBL && ops[j].Value == op.Value)
                            {
                                returnLines.Add(i);
                                i = j;
                                foundJmpLabel4 = true;
                            }
                        }
                        if (!foundJmpLabel4)
                        {
                            PrintDebugInfo();
                            return RS.LABEL_NOT_FOUND;
                        }

                        SaveDebugInfo(i);
                        break;

                    case OC.RET:
                        if (returnLines.Count == 0)
                        {
                            PrintDebugInfo();
                            return RS.NO_RETURN_POINT;
                        }
                        SaveDebugInfo(i);
                        i = returnLines[returnLines.Count - 1];
                        returnLines.RemoveAt(returnLines.Count - 1);
                        break;

                    case OC.END:
                        SaveDebugInfo(i);
                        PrintDebugInfo();
                        return RS.OK;


                    default:
                        return RS.UNRECOGNISED_OPCODE;
                }
            }

            PrintDebugInfo();
            return RS.OK;
        }

        private int GetValueAtBoxAddress(int num)
        {
            foreach (var val in boxValues)
            {
                if (val.Key == num) return val.Value;
            }
            return 0;
        }

        private void SaveDebugInfo(int index)
        {
            boxValues = boxValues.OrderBy(x => x.Key).ToList();
            StringBuilder line = new StringBuilder();
            StringBuilder asmLine = new StringBuilder();

            line.Append(ops[index].GetRawCode());
            for (int i = 0; i < TAB_GAP - ops[index].GetRawCode().Length ; i++)
            {
                line.Append(" ");
            }

            asmLine.Append(ops[index].Code + " ");
            if (ops[index].InputType == 2) asmLine.Append("(");
            asmLine.Append(ops[index].Value);
            if (ops[index].InputType == 2) asmLine.Append(")");
            line.Append(asmLine);

            for (int i = 0; i < TAB_GAP - asmLine.Length - 4; i++)
            {
                line.Append(" ");
            }


            line.Append($"A {addValue}");
            for (int i = 0; i < (10 - addValue.ToString().Length); i++)
            {
                line.Append(" ");
            }
            line.Append("B ");
            foreach (var boxVal in boxValues)
            {
                line.Append($"{boxVal.Key}:{boxVal.Value}");
                for (int i = 0; i < 10 - (boxVal.Key.ToString().Length + boxVal.Value.ToString().Length); i++)
                {
                    line.Append(" ");
                }
            }

            debugLines.Add(line.ToString());
        }
        private void PrintDebugInfo()
        {
            if (debug)
            {
                InOut.WriteLine("\n\nParsed Code:\n");
                foreach (var item in parsedLines)
                {
                    InOut.WriteLine(item);
                }
                InOut.WriteLine();

                InOut.WriteLine("Executed Code:\n");
                foreach (var item in debugLines)
                {
                    InOut.WriteLine(item);
                }
                InOut.WriteLine();
            }
            else Console.WriteLine("\n");
        }
    }
}
